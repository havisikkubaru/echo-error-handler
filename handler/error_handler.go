package handler

import (
	"net/http"

	"echo-error-handler/exceptions"

	"github.com/labstack/echo/v4"
)

/*
	Fungsi untuk membuat custom error.
*/
func CustomErrorHandling(err error, c echo.Context) {
	if notFoundError(err, c) {
		return
	} else if badRequestError(err, c) {
		return
	} else if forbiddenError(err, c) {
		return
	} else {
		internalServerError(err, c)
		return
	}
}

func internalServerError(err error, c echo.Context) bool {
	response, ok := err.(*exceptions.InternalServerErrorStruct)
	if ok {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"success": false,
			"message": response.Error(),
		})
		return true
	}
	return false
}

func badRequestError(err error, c echo.Context) bool {
	response, ok := err.(*exceptions.BadRequestErrorStruct)
	if ok {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"success": false,
			"message": response.Error(),
		})
		return true
	}
	return false
}

func notFoundError(err error, c echo.Context) bool {
	response, ok := err.(*exceptions.NotFoundErrorStruct)
	if ok {
		c.JSON(http.StatusNotFound, map[string]interface{}{
			"success": false,
			"message": response.Error(),
		})
		return true
	}
	return false
}

func forbiddenError(err error, c echo.Context) bool {
	response, ok := err.(*exceptions.ForbiddenErrorStruct)
	if ok {
		c.JSON(http.StatusForbidden, map[string]interface{}{
			"success": false,
			"message": response.Error(),
		})
		return true
	}
	return false
}
