package handler

import (
	"echo-error-handler/exceptions"
	"net/http"

	"github.com/labstack/echo/v4"
)

func ErrorService(c echo.Context) error {
	/*
		Ambil query parameter dengan key "error"
	*/
	errorParam := c.QueryParam("error")

	switch errorParam {
	case "internal server error":
		return exceptions.NewInternalServerError(errorParam)
	case "bad request":
		return exceptions.NewBadRequestError(errorParam)
	case "not found":
		return exceptions.NewNotFoundError(errorParam)
	case "forbidden":
		return exceptions.NewForbiddenError(errorParam)
	}

	return c.JSON(http.StatusOK, map[string]interface{}{
		"success": true,
		"message": "no error occured",
	})
}
