package main

import (
	"log"

	"echo-error-handler/handler"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func main() {
	/*
		Inisialisasi framework echo
	*/
	e := echo.New()

	/*
		Timpa fungsi HTTPErrorHandler dari echo,
		menggunakan custom errorHandler buatan sendiri.
	*/
	e.HTTPErrorHandler = handler.CustomErrorHandling

	/*
		Untuk mengabaikan ketika terjadi sebuah error.
	*/
	e.Use(middleware.Recover())

	e.GET("/service", handler.ErrorService)

	err := e.Start(":8080")
	if err != nil {
		log.Fatal(err)
	}
}
